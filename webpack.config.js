var path = require("path");
var webpack = require("webpack");
var normalize = require('normalize');

module.exports = {
	cache: true,
	entry: {
		workbench: "./src/workbench"
	},
	output: {
		path: path.join(__dirname, "lib"),
		publicPath: "lib/",
		filename: "[name].js",
		chunkFilename: "[chunkhash].js"
	},
	module: {
		loaders: [
			{ test: /\.coffee$/, loader: "coffee" },
			{ test: /\.css$/, loader: "style-loader!css-loader" },
			{ test: /\.jade$/, loader: "jade" },
			{ test: /\.styl$/, loader: "style-loader!css-loader!stylus-loader" },
			{ test: /\.png$/, loader: "url-loader?limit=100000" },
			{ test: /\.jpg$/, loader: "url-loader?limit=100000" },
			{ test: /\.svg$/, loader: "url-loader?limit=100000" },
		]
	},
	externals: [
		function(context, request, callback) {
			if(/^(core|spine|editor)/.test(request))
				return callback(null, 'window.require("' + request + '")');
			callback();
		},
	],
	resolve: {
		modulesDirectories: ['node_modules', 'bower_components'],
		extensions: ["", ".coffee", ".js", ".styl", ".jade"],		
		alias: {
		}
	},
	stylus: {
		use: [normalize()]
	},
	plugins: [
		new webpack.ResolverPlugin(
			new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin("bower.json", ["main"])
		),
	]
}
