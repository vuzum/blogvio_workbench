# The Widget Workbench

## Wishlist

* linting / validation of meta json
* refresh presets.json
* buton pt toggle autoscale
* embed test panel
    - page with realistic templates with various embed placements
* skin/content migration test panel
    - test the current widget code with the presets & demo content from previous releases, to see if it still loads
    - previous versions are gathered from git (merged demo/presets.json logs)
    - button to run through tests automatically
        + auto-detection of errors
