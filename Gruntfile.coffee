module.exports = (grunt) ->
	require("matchdep").filterAll("grunt-*").forEach(grunt.loadNpmTasks)

	webpack = require("webpack")
	webpackConfig = require("./webpack.config.js")
	grunt.initConfig
		webpack: 
			options: webpackConfig
			build:
				plugins: webpackConfig.plugins.concat(
					new webpack.optimize.DedupePlugin()
					new webpack.optimize.UglifyJsPlugin()
				)

			"build-dev":
				devtool: "sourcemap"
				debug: true

		"webpack-dev-server":
			options:
				webpack: webpackConfig
				publicPath: "/" + webpackConfig.output.publicPath
				port: 8082
			start:
				keepAlive: true
				webpack:
					devtool: "sourcemap"
					debug: true

		watch:
			app:
				files: ["src/**/*", "css/**/*"]
				tasks: ["webpack:build-dev"]
				options:
					spawn: false

	grunt.registerTask("default", ["webpack-dev-server:start"])
	grunt.registerTask("dev", ["webpack:build-dev", "watch:app"])
	grunt.registerTask("build", ["webpack:build"])

