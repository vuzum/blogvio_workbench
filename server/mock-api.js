"use strict"
var express = require('express')
var path    = require('path')
var fs      = require('fs');

function mockApi(presetsJson) {
  var guid = 0
  var compositions = []
  var skins = JSON.parse(fs.readFileSync(presetsJson));

  var app = express()

  app.set('views', path.join(__dirname, '/views'))
  app.set('view engine', 'jade')

  // handle url checks
  app.post('/compositions', function(req, res) {
    let composition = req.body
    composition.id = String(++guid)
    composition.user_id = 'mock-user'
    composition.content.map(c => c.composition_id = composition.id)

    compositions.push(composition)

    res.json(composition)
  })

  app.post('/compositions/:id', function(req, res) {
    compositions = compositions.map(function(comp) {
      if(comp.id === req.params.id) return req.body
      return comp
    })

    res.json(req.body)
  })

  app.get('/compositions/:id', function(req, res) {
    res.json(compositions.filter(function(comp) { return comp.id === req.params.id })[0])
  })

  app.get('/compositions/:id/embed.html', function(req, res) {
    var composition = compositions.filter(function(comp) { return comp.id === req.params.id })[0]
    var skin = skins.filter(function(skin) { return skin.id === composition.skin_id})[0]
    res.render('embed', {composition: composition, skin: skin})
  })

  app.get('/skins', function(req, res) {
    res.json(skins)
  });

  app.post('/skins', function(req, res) {
    req.body.id = String(++guid)
    req.body.user_id = 'mock-user'

    skins.push(req.body)

    res.json(req.body)
  })

  app.post('/skins/:id', function(req, res) {
    skins = skins.map(function(skin) {
      if(skin.id === req.params.id) return req.body
      return skin
    })

    res.json(req.body)
  })

  return app
}

module.exports = mockApi
