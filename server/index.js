// Node
var util = require('util');

// Webpack
var webpack           = require('webpack');
var WebpackDevServer  = require('webpack-dev-server');

// Express
var express = require('express');
var morgan  = require('morgan');
var bodyParser = require('body-parser');

// Express App
var app = express();
var httpProxy = require('http-proxy');
var http = require('http');
var server = http.createServer(app);

// Env
var PORT     = process.env.PORT || 8082;
var NODE_ENV = process.env.NODE_ENV || 'development';

// log requests to console
app.use(morgan('dev'));

app.use(bodyParser.json())

// serve the workbench files
app.use(express.static(__dirname + '/../public'));

// handle url checks
app.get('/api/v2/urls/*', function(req, res) {
  res.setHeader('Content-Type', 'application/json');
  response = {"success":true, "type":"audio\/mpeg", "statusCode":200};
  res.send(response)
});

server.listen(PORT, function() {
  console.log('Listen on http://localhost:' + PORT + ' in ' + NODE_ENV);
});

module.exports = {
  proxyWidgeticApi: function(auth) {
    // proxy some requests to the widgetic server
    var apiProxy = httpProxy.createProxyServer();
    var proxyWidgetic = function(req, res){
      apiProxy.web(req, res, {
        target: 'https://widgetic.com',
        changeOrigin: true,
        secure: false,
      });
    }

    app.get("/bundles/*", proxyWidgetic);
    app.get("/assets/*", proxyWidgetic);

    apiProxy.on('error', function (err, req, res) {
      res.writeHead(500, {
        'Content-Type': 'text/plain'
      });

      res.end('Something went wrong. And we are reporting a custom error message.');
    });
  },
  mockApi: function(presetsJson) {
    var mockApi = require('./mock-api')
    app.use('/api/v2', mockApi(presetsJson))
  },
  use: function() {
    app.use.apply(app, arguments)
  },
  startWebpack: function(config) {
    // use webpack-dev-server to serve the compiled scripts
    // it also serves all the files in the project directory
    // including bower_components

    // define DEVELOPMENT
    config.plugins.push(new webpack.DefinePlugin({
      DEVELOPMENT: true,
      PRODUCTION: false,
    }))
    var wserver = new WebpackDevServer(webpack(config), {
      publicPath: '/lib',
      inline: true,
      quiet: false,
      noInfo: false,
      stats: { colors: true }
    });
    app.use(wserver.app);

    // link the auto-relaod server
    var io = require("socket.io").listen(server);
    wserver.io = io;
    io.on("connection", function(socket) {
      if(this.hot) socket.emit("hot");
      if(!this._stats) return;
      this._sendStats(socket, this._stats.toJson(), true);
    }.bind(wserver));
  },
  loadBackend: function(backend) {
    require('./backend')(app, backend)
  }
}
