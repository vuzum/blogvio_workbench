require '../../css/style.css'

Widgetic.init('bogus', 'bogus')
Widgetic.auth.token('bogus')
$ ->
	wel = $('<div>')
	$('body').append wel
	w = new Workbench el: wel

Controller = require('spine/controller')

class Workbench extends Controller
	template: require './workbench.jade'
	elements:
		'.widget-area': 'widgetArea'
		'.editor-area': 'editorArea'
		'.resize-area': 'resizeArea'
		'.workbench-widget-size-w': 'widgetW'
		'.workbench-widget-size-h': 'widgetH'
		'.workbench-preset-output': 'presetOutput'
		'.workbench-preset-output ~ span': 'presetCopyMessage'
		'.workbench-embed-code-output': 'embedCodeOutput'
		'.workbench-embed-code-output ~ span': 'embedCodeCopyMessage'
		'[data-action="copyEmbedCode"]': 'copyEmbedCodeButton'
	events:
		'mouseenter .resize-area-toggle': '_onResizeEnter'
		'mouseup .resize-area': '_onMouseUp'
		'mousedown .resize-area': '_onMouseDown'
		'mousemove': '_onMouseMove'
		'mousemove  .resize-area': '_onResizeDrag'
		'click [data-action]': '_onAction'
		'change input': '_onChange'

	constructor: (options) ->
		super

		@render()

	render: =>
		@html(@template(@))
		@initSize()
		@initComposition()
		@initEditor()

	initSize: ->
		@widgetArea.width  localStorage.workbenchWidth  or 400
		@widgetArea.height localStorage.workbenchHeight or 300
		@_updateDimensions()

	initComposition: (widget) ->
		if @initWithSavedComposition
			@composition = Widgetic.UI.composition(@widgetArea[0], @savedComposition.id)
		else
			@composition = Widgetic.UI.composition(@widgetArea[0], { widget_id: 'widget', edit_mode: true });

		@resizeArea.resizable
			handles: 'se'
			alsoResize: @widgetArea

	initEditor: (widget) ->
		@editor = Widgetic.UI.editor(@editorArea[0], @composition)
		@editor.on 'composition:save', (@savedComposition) =>
			@render()


	resetWorkbench: ->
		localStorage.clear()
		window.location.reload()

	loadSavedComposition: ->
		@initWithSavedComposition = true
		@render()

	copyPreset: ->
		preset = this.editor._iframe.contentWindow.require('core/models/composition').first().getSkin()
		preset = JSON.stringify(preset, null, '\t')
		@presetOutput.val(preset)
		@presetOutput[0].select()

		try
			successful = document.execCommand('copy')
			if successful
				@presetCopyMessage.hide()
					.fadeIn('fast')
					.delay(1000)
					.fadeOut('normal')
		catch err
			console.error('Oops, unable to copy', err)

	copyEmbedCode: ->
		code = """
			<script>
				window.widgeticOptions = {
				  domain: 'local.widgetic.com:8082',
				  secure: false,
				}
			</script>
			<script async src="http://local.widgetic.com:8082/node_modules/blogvio_workbench/node_modules/widgeticjs/lib/sdk.dev.js"></script>
			<a href="#" class="widgetic-composition" data-id="#{ @savedComposition.id }" data-width="#{ @widgetW.val() }" data-height="#{ @widgetH.val() }" data-brand="bottom-right">
		"""
		@embedCodeOutput.val(code)
		@embedCodeOutput[0].select()

		try
			successful = document.execCommand('copy')
			if successful
				@embedCodeCopyMessage.hide()
					.fadeIn('fast')
					.delay(1000)
					.fadeOut('normal')
		catch err
			console.error('Oops, unable to copy', err)

	_onAction: (ev) ->
		ev.preventDefault();
		$this = $(ev.target);
		@[$this.data('action')]($this, ev);

	_onResizeEnter: (ev) ->
		@resizeArea.addClass 'resize-area--active'
		@resizeIntent = true
		@resizeInitalPosition = {
			x: event.pageX,
			y: event.pageY
		}

	_onMouseUp: =>
		@resizeIntent = false
		@resizeArea.removeClass 'resize-area--active'
		@_updateDimensions()

	_onMouseDown: (ev) =>
		@resizeIntent = false

	_onMouseMove: (ev) ->
		return unless @resizeIntent

		currentPosition = {
			x: event.pageX,
			y: event.pageY
		}
		@_onMouseUp() if Math.abs(@resizeInitalPosition.x - currentPosition.x) > 20
		@_onMouseUp() if Math.abs(@resizeInitalPosition.y - currentPosition.y) > 20

	_onResizeDrag: (ev) ->
		return unless @resizeArea.hasClass 'resize-area--active'
		@_updateDimensions()

	_onChange: (ev) ->
		@widgetArea.width  @widgetW.val()
		@widgetArea.height @widgetH.val()
		@_cacheDimensions()

	_updateDimensions: ->
		@widgetW.val @widgetArea.width()
		@widgetH.val @widgetArea.height()
		@_cacheDimensions()

	_cacheDimensions: ->
		localStorage.workbenchWidth  = @widgetW.val()
		localStorage.workbenchHeight = @widgetH.val()
